module bitbucket.org/peter1003/slotorm

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang-migrate/migrate/v4 v4.12.2
	github.com/jinzhu/gorm v1.9.16
	github.com/urfave/cli v1.22.4
	github.com/urfave/cli/v2 v2.2.0
)
