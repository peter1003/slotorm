package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"os/exec"
	"path"
	"runtime"
)

func main() {

	_, filename, _, _ := runtime.Caller(0)
	migrationRoot := path.Join(path.Dir(filename), "/src/migrations")

	connectStr := "mysql://" + os.Getenv("DB_USERNAME") + ":" + os.Getenv("DB_PASSWORD") + "@tcp(" + os.Getenv("DB_HOST") + ":" + os.Getenv("DB_PORT") + ")/" + os.Getenv("DB_DATABASE") + "?query"
	app := &cli.App{
		Commands: []*cli.Command{
			{
				Name:    "migrate",
				Aliases: []string{"m"},
				Usage:   "do migrate task",
				Subcommands: []*cli.Command{
					{
						Name:    "create",
						Aliases: []string{"c"},
						Usage:   "create new migrate",
						Action: func(c *cli.Context) error {
							execCmd("migrate", "create", "-ext", "sql", "-dir", migrationRoot, c.Args().First())
							return nil
						},
					},
					{
						Name:    "up",
						Aliases: []string{"u"},
						Usage:   "migrate up",
						Action: func(c *cli.Context) error {

							execCmd("migrate", "-source", "file://"+migrationRoot, "-database", connectStr, "up")
							return nil
						},
					},
					{
						Name:    "down",
						Aliases: []string{"d"},
						Usage:   "migrate down",
						Action: func(c *cli.Context) error {
							execCmd("migrate", "-source", "file://"+migrationRoot, "-database", connectStr, "down", "1")
							return nil
						},
					},
					{
						Name:    "force",
						Aliases: []string{"f"},
						Usage:   "migrate force",
						Action: func(c *cli.Context) error {
							execCmd("migrate", "-source", "file://"+migrationRoot, "-database", connectStr, "force", c.Args().First())
							return nil
						},
					},
				},
			},
			{
				Name:    "complete",
				Aliases: []string{"c"},
				Usage:   "complete a task on the list",
				Action: func(c *cli.Context) error {
					fmt.Println("completed task: ", c.Args().First())
					return nil
				},
			},
			{
				Name:    "template",
				Aliases: []string{"t"},
				Usage:   "options for task templates",
				Subcommands: []*cli.Command{
					{
						Name:  "add",
						Usage: "add a new template",
						Action: func(c *cli.Context) error {
							fmt.Println("new task template: ", c.Args().First())
							return nil
						},
					},
					{
						Name:  "remove",
						Usage: "remove an existing template",
						Action: func(c *cli.Context) error {
							fmt.Println("removed task template: ", c.Args().First())
							return nil
						},
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func execCmd(cmdArgs ...string) {

	cmd := exec.Command(cmdArgs[0], cmdArgs[1:len(cmdArgs)]...)
	// fmt.Println(cmdArgs)
	output, err := cmd.CombinedOutput()

	if err != nil {
		log.Println(err)
	} else {
		log.Println(string(output))
	}

}
