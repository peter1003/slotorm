package db

import (
	"background_api/src/utils/config"
	"background_api/src/utils/logger"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"sync"
)

var (
	lock         sync.Mutex
	db       *gorm.DB
	interval = config.GetInt("db.interval")
	dialect  = config.GetString("db.dialect")
	source   = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?%s",
		config.Get("db.user"),
		config.Get("db.password"),
		config.Get("db.host"),
		config.Get("db.port"),
		config.Get("db.database"),
		config.Get("db.flag"),
	)
)

func init() {
	go connectionPool()
}

// DB return database instance
func DB() *gorm.DB {
	if db != nil {
		return db
	}

	lock.Lock()
	defer lock.Unlock()

	if db != nil {
		return db
	}
	db.LogMode(config.GetBool("db.isShowConsole"))
	if db == nil {
		connect(dialect, source)
	}
	return db
}

// Close close database connection
func Close() {
	if db != nil {
		err := db.Close()
		if err != nil {
			logger.ApplicationErrorSimple("gorm close error", err)
		}
	}
}

func connect(dialect string, source string) {
	dbInstance, err := gorm.Open(dialect, source)

	if err != nil {
		logger.ApplicationErrorSimple("gorm connection open error", err)
	} else {
		dbInstance.SingularTable(true)
		dbInstance.BlockGlobalUpdate(true)
		dbInstance.Exec("SET @@global.wait_timeout=300")
		dbInstance.Exec("SET @@session.wait_timeout=300")
		dbInstance.DB().SetConnMaxLifetime(time.Duration(interval) * time.Second)
		dbInstance.DB().SetMaxIdleConns(200)
		dbInstance.DB().SetMaxOpenConns(200)

		db = dbInstance
	}
}

func connectionPool() {
	for {
		if err := DB().DB().Ping(); err != nil {
			logger.ApplicationErrorSimple("Error ping to DB", err)
			if db != nil {
				dbErr := db.Close()
				if dbErr != nil {
					logger.ApplicationErrorSimple("close db after ping db error", dbErr)
				}
			}
			connect(dialect, source)
		}
		time.Sleep(time.Duration(interval) * time.Second)
	}
}
